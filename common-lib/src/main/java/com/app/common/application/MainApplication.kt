package com.app.common.application

import android.app.Application

abstract class MainApplication : Application(), IApplication {

    companion object {
        private var instance: MainApplication? = null
        fun getInstance(): IApplication? = instance

    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }

}