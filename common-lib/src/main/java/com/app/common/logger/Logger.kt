package com.app.common.logger

import android.util.Log
import com.app.common.BuildConfig
import com.app.common.application.IApplication
import com.app.common.application.MainApplication

fun logD(message: String) {
    if (BuildConfig.DEBUG) {
        Log.d("COMM_LOGGER", message)
    }
}