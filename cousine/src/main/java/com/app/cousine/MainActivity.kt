package com.app.cousine

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import com.app.cousine.ui.navigation.CousineNavigation
import com.app.cousine.ui.theme.MetaWeatherTheme

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            MetaWeatherTheme {
                Surface(color = MaterialTheme.colors.background) {
                    CousineNavigation()
                }
            }
        }
    }
}

