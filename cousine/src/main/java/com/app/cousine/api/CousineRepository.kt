package com.app.cousine.api

import com.app.network.configuration.NetworkConfig

object CousineRepository {

    private val service = NetworkConfig.buildService(CousineService::class.java)

    suspend fun getCousineCategories() = service.getCategories()

    suspend fun getMealByCategory(categoryType: String) = service.getMealByCategory(categoryType)

}