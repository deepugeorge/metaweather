package com.app.cousine.api

import com.app.cousine.model.Categories
import com.app.cousine.model.MealList
import com.app.network.model.ApiError
import com.app.network.result.Result
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface CousineService {

    @GET("categories.php")
    suspend fun getCategories(): Result<Categories, ApiError>

    @GET("filter.php")
    suspend fun getMealByCategory(@Query("c") categoryType: String): Result<MealList, ApiError>
}