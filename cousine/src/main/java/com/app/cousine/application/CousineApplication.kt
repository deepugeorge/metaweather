package com.app.cousine.application

import com.app.cache.LocalCache
import com.app.common.application.MainApplication

class CousineApplication : MainApplication() {

    private var instance: CousineApplication? = null

    override var baseUrl: String = "https://www.themealdb.com/api/json/v1/1/"

    override fun onCreate() {
        instance = this
        super.onCreate()
    }

    override fun onTerminate() {
        super.onTerminate()
        LocalCache.clear()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        LocalCache.clear()
    }


}