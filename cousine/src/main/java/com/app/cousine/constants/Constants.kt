package com.app.cousine.constants

import com.app.cache.CacheKey

enum class FoodieCache : CacheKey {
    CATEGORY_LIST
}