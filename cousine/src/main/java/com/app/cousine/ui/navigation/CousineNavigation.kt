package com.app.cousine.ui.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.app.cousine.ui.screen.CategoryDetails
import com.app.cousine.ui.screen.CategoryHomePage
import com.app.cousine.ui.screen.CousineHomePage
import com.app.ui.view.popBack

@Composable
fun CousineNavigation() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "cousine_list") {
        composable("cousine_list") {
            CousineHomePage { categoryId ->
                navController.navigate("category_details/$categoryId")
            }
        }

        composable(
            "category_details/{categoryId}",
            arguments = listOf(navArgument("categoryId") {
                type = NavType.StringType
            })
        ) {

            CategoryDetails(it.arguments?.getString("categoryId").orEmpty(),
                { popBack(navController) }) { categoryType ->
                navController.navigate("category_list/$categoryType")
            }
        }

        composable("category_list/{categoryType}", arguments = listOf(navArgument("categoryType") {
            type = NavType.StringType
        })) {
            CategoryHomePage(
                it.arguments?.getString("categoryType").orEmpty()
            ) { popBack(navController) }
        }
    }
}