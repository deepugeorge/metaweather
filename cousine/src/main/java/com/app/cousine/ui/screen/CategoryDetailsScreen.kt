package com.app.cousine.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import com.app.cousine.R
import com.app.cousine.model.Category
import com.app.cousine.viewmodel.CategoryDetailsViewModel
import com.app.cousine.viewmodel.CategoryDetailsViewModelFactory

@Composable
fun CategoryDetails(
    categoryId: String,
    onBackPressed: () -> Unit,
    navigateToMeals: (categoryId: String) -> Unit
) {
    val viewModel: CategoryDetailsViewModel = viewModel(factory = CategoryDetailsViewModelFactory())
    val category = viewModel.getSelectedCategory(categoryId)
    Scaffold(topBar = {
        TopAppBar(
            title = {
                Text(text = category?.categoryType.orEmpty())
            },
            navigationIcon = {
                IconButton(onClick = { onBackPressed() }) {
                    Icon(Icons.Filled.ArrowBack, stringResource(id = R.string.category))
                }
            },
            backgroundColor = MaterialTheme.colors.background,
            contentColor = MaterialTheme.colors.contentColorFor(MaterialTheme.colors.background),
            elevation = 12.dp
        )
    },
        floatingActionButton = {
            FloatingActionButton(onClick = { navigateToMeals(category?.categoryType.orEmpty()) }) {
                Icon(Icons.Filled.ArrowForward, "")
            }
        }

    ) {
        CategoryDetailsItemContainer(category)
    }
}

@Composable
fun CategoryDetailsItemContainer(category: Category?) {

    val scrollState = rememberScrollState()
    Column(
        modifier = Modifier
            .verticalScroll(scrollState)
            .padding(10.dp)
            .fillMaxWidth()
    ) {
        Image(
            painter = rememberImagePainter(data = category?.imageUrl),
            contentDescription = "",
            modifier = Modifier
                .size(300.dp)
                .align(Alignment.CenterHorizontally)
        )

        Text(
            text = category?.description.orEmpty(),
            style = MaterialTheme.typography.subtitle1,
            color = MaterialTheme.colors.secondaryVariant
        )
    }
}