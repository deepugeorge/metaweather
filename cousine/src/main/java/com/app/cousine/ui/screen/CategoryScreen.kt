package com.app.cousine.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.app.cousine.R
import com.app.cousine.model.Meal
import com.app.cousine.viewmodel.MealByCategoryViewModel
import com.app.cousine.viewmodel.MealByCategoryViewModelFactory
import com.app.ui.view.NetworkResponsiveView

@Composable
fun CategoryHomePage(categoryType: String, onBackPressed: () -> Unit) {
    Scaffold(topBar = {
        TopAppBar(
            title = {
                Text(text = categoryType)
            },
            navigationIcon = {
                IconButton(onClick = { onBackPressed() }) {
                    Icon(Icons.Filled.ArrowBack, stringResource(id = R.string.category))
                }
            },
            backgroundColor = MaterialTheme.colors.background,
            contentColor = MaterialTheme.colors.contentColorFor(MaterialTheme.colors.background),
            elevation = 12.dp
        )
    }) {
        CategoryType(categoryType)
    }
}

@Composable
fun CategoryType(categoryType: String) {
    val viewModel: MealByCategoryViewModel =
        viewModel(factory = MealByCategoryViewModelFactory(categoryType))
    val meals = viewModel.meals
    NetworkResponsiveView(viewModel = viewModel) {
        LazyColumn(contentPadding = PaddingValues(10.dp)) {
            items(meals) { meal ->
                MealItem(meal = meal)
            }
        }
    }
}

@Preview
@Composable
fun MealItem(meal: Meal = Meal("1", "Chicken", "imgurl")) {
    Card(elevation = 2.dp, modifier = Modifier
        .padding(top = 10.dp)
        .clickable {
        }) {
        Row(
            Modifier
                .padding(20.dp)
                .fillMaxWidth()
        ) {
            Image(
                painter = rememberImagePainter(meal.imageUrl, builder = {
                    transformations(CircleCropTransformation())
                }),
                contentDescription = stringResource(id = R.string.image_description),
                Modifier
                    .size(60.dp)
                    .align(Alignment.CenterVertically)
            )

            Text(
                text = meal.name,
                style = MaterialTheme.typography.h6,
                color = MaterialTheme.colors.primaryVariant,
                textAlign = TextAlign.Left,
                maxLines = 1,
                modifier = Modifier
                    .padding(start = 10.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically)
            )
        }

    }

}