package com.app.cousine.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Home
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import coil.compose.rememberImagePainter
import coil.transform.CircleCropTransformation
import com.app.cousine.R
import com.app.cousine.model.Category
import com.app.cousine.viewmodel.CategoryListViewModel
import com.app.cousine.viewmodel.CategoryListViewModelFactory
import com.app.ui.view.NetworkResponsiveView

@Composable
fun CousineHomePage(onItemSelected: ((categoryId: String) -> Unit?)? = null) {

    Scaffold(topBar = {
        TopAppBar(
            title = {
                Text(text = stringResource(id = R.string.app_name))
            },
            navigationIcon = {
                IconButton(onClick = { }) {
                    Icon(Icons.Filled.Home, stringResource(id = R.string.app_name))
                }
            },
            backgroundColor = MaterialTheme.colors.background,
            contentColor = MaterialTheme.colors.contentColorFor(MaterialTheme.colors.background),
            elevation = 12.dp
        )
    }) {
        CousineCategory {
            onItemSelected?.invoke(it)
        }
    }


}

@Composable
fun CousineCategory(onItemSelected: ((categoryId: String) -> Unit?)? = null) {
    val categoryListViewModel: CategoryListViewModel =
        viewModel(factory = CategoryListViewModelFactory())
    val categories = categoryListViewModel.categories
    NetworkResponsiveView(viewModel = categoryListViewModel) {
        LazyColumn(contentPadding = PaddingValues(10.dp)) {
            items(categories) { category ->
                CousineCategoryItem(category = category, onItemSelected)
            }
        }
    }

}

@Preview
@Composable
fun CousineCategoryItem(
    category: Category = Category("1", "Chicken", "", ""),
    onItemSelected: ((categoryId: String) -> Unit?)? = null
) {
    Card(elevation = 2.dp, modifier = Modifier
        .padding(top = 10.dp)
        .clickable {
            onItemSelected?.invoke(category.id)
        }) {
        Row(
            Modifier
                .padding(20.dp)
                .fillMaxWidth()
        ) {
            Image(
                painter = rememberImagePainter(category.imageUrl, builder = {
                    transformations(CircleCropTransformation())
                }),
                contentDescription = stringResource(id = R.string.image_description),
                Modifier
                    .size(60.dp)
                    .align(Alignment.CenterVertically)
            )
            Column(
                modifier = Modifier
                    .padding(start = 10.dp)
                    .fillMaxWidth()
                    .align(Alignment.CenterVertically)
            ) {
                Text(
                    text = category.categoryType,
                    style = MaterialTheme.typography.h6,
                    color = MaterialTheme.colors.primaryVariant,
                    textAlign = TextAlign.Left
                )

                Text(
                    text = category.description,
                    style = MaterialTheme.typography.subtitle2,
                    color = MaterialTheme.colors.primary,
                    maxLines = 2,
                    textAlign = TextAlign.Left,
                    modifier = Modifier.padding(top = 5.dp)
                )
            }

        }
    }
}

@Preview
@Composable
fun CousineListItemPreview() {
    CousineHomePage()
}
