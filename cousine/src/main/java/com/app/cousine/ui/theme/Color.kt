package com.app.cousine.ui.theme

import androidx.compose.ui.graphics.Color


val primaryColor = Color(0xFF0097a7)
val primaryLightColor = Color(0xFF56c8d8)
val primaryDarkColor = Color(0xFF006978)
val secondaryColor = Color(0xFF004d40)
val secondaryLightColor = Color(0xFF39796b)
val secondaryDarkColor = Color(0xFF00251a)
val primaryTextColor = Color(0xFF000000)
val secondaryTextColor = Color(0xFFffffff)