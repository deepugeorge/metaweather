package com.app.cousine.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.app.cache.Cache
import com.app.cache.LocalCache
import com.app.cousine.constants.FoodieCache
import com.app.cousine.model.Category

class CategoryDetailsViewModel(private val cache: Cache) : ViewModel() {

    fun getSelectedCategory(categoryId: String): Category? {
        val result = cache.getResult<FoodieCache, List<Category>>(FoodieCache.CATEGORY_LIST)
        return result?.findLast { it.id == categoryId }
    }
}

class CategoryDetailsViewModelFactory :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CategoryDetailsViewModel::class.java)) {
            return CategoryDetailsViewModel(LocalCache) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}