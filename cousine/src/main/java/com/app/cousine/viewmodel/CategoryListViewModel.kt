package com.app.cousine.viewmodel

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.app.cache.Cache
import com.app.cache.LocalCache
import com.app.cousine.api.CousineRepository
import com.app.cousine.constants.FoodieCache
import com.app.cousine.model.Category
import com.app.network.result.Result
import com.app.ui.state.NetworkState
import com.app.ui.state.StateManager
import com.app.ui.state.StateManagerViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class CategoryListViewModel(
    private val repository: CousineRepository,
    private val cache: Cache,
    dispatcher: CoroutineDispatcher = Dispatchers.Default
) : ViewModel(), StateManagerViewModel {

    private val _networkState: MutableStateFlow<NetworkState> = MutableStateFlow(NetworkState())

    override var networkState: StateFlow<NetworkState> = _networkState


    private val stateManager = StateManager(_networkState)

    var categories = mutableStateListOf<Category>()
        private set


    init {
        viewModelScope.launch(dispatcher) {
            stateManager.showLoading()
            val result = repository.getCousineCategories()
            if (result is Result.Success) {
                val categoryList = result.body.categories
                cache.save(FoodieCache.CATEGORY_LIST, categoryList)
                categories.addAll(categoryList)
            }
            stateManager.mangeResult(result)
        }

    }


}

class CategoryListViewModelFactory :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CategoryListViewModel::class.java)) {
            return CategoryListViewModel(CousineRepository, LocalCache) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}