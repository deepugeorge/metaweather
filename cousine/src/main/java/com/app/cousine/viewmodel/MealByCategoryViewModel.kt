package com.app.cousine.viewmodel

import androidx.compose.runtime.mutableStateListOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.app.cousine.api.CousineRepository
import com.app.cousine.model.Meal
import com.app.network.result.Result
import com.app.ui.state.NetworkState
import com.app.ui.state.StateManager
import com.app.ui.state.StateManagerViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch

class MealByCategoryViewModel(
    categoryType: String,
    cousineRepository: CousineRepository,
    dispatcher: CoroutineDispatcher = Dispatchers.Default
) : ViewModel(), StateManagerViewModel {
    private val _networkState: MutableStateFlow<NetworkState> = MutableStateFlow(NetworkState())

    override var networkState: StateFlow<NetworkState> = _networkState

    private val stateManager = StateManager(_networkState)

    var meals = mutableStateListOf<Meal>()
        private set

    init {
        viewModelScope.launch(dispatcher) {
            stateManager.showLoading()
           val result =  cousineRepository.getMealByCategory(categoryType)
            if(result is Result.Success) {
                meals.addAll(result.body.meals)
            }
            stateManager.mangeResult(result)
        }
    }
}

class MealByCategoryViewModelFactory(private val categoryType: String) :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MealByCategoryViewModel::class.java)) {
            return MealByCategoryViewModel(categoryType, CousineRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}