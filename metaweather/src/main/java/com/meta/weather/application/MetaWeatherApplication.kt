package com.meta.weather.application

import com.app.common.application.MainApplication

class MetaWeatherApplication : MainApplication() {

    private var instance:MetaWeatherApplication?= null


    override fun onCreate() {
        instance = this
        super.onCreate()
    }


    override var baseUrl: String = "https://www.metaweather.com/api/"
}