package com.meta.weather.landing

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import com.google.accompanist.pager.ExperimentalPagerApi
import com.meta.weather.landing.compose.WeatherOverview
import com.meta.weather.landing.viewmodel.MetaWeatherViewModel
import com.meta.weather.landing.viewmodel.MetaWeatherViewModelFactory
import com.meta.weather.ui.theme.MetaWeatherTheme

class MetaWeatherActivity : ComponentActivity() {
    @ExperimentalPagerApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            MetaWeatherTheme {
                Surface(color = MaterialTheme.colors.background) {
                    WeatherOverview()
                }
            }
        }
    }
}
