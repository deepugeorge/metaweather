package com.meta.weather.landing.api

import java.text.SimpleDateFormat
import java.util.*

const val IMAGE_URL = "https://www.metaweather.com/static/img/weather/png/64/%s.png"
val WEATHER_DATE_FORMAT = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
val TARGET_DATE_FORMAT = SimpleDateFormat("EEEE, MMM d", Locale.ENGLISH)