package com.meta.weather.landing.api


data class MetaError(
    val errorMessage: String
)