package com.meta.weather.landing.api

import com.app.network.configuration.NetworkConfig

object WeatherRepository {
    private val service = NetworkConfig.buildService(WeatherService::class.java)

    suspend fun getWeatherOverviewList(woeid: Int) = service.fetchWeatherDetails(woeid = woeid)
}