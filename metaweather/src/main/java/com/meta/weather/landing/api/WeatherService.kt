package com.meta.weather.landing.api

import com.app.network.result.Result
import com.meta.weather.landing.model.ConsolidatedWeather
import retrofit2.http.GET
import retrofit2.http.Path

interface WeatherService {
    @GET("location/{woeid}")
    suspend fun fetchWeatherDetails(@Path("woeid") woeid: Int): Result<ConsolidatedWeather, MetaError>
}