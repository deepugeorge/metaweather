package com.meta.weather.landing.compose

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.meta.weather.R

@Composable
@Preview
fun Humidity(value: Int = 65) {
    Row(modifier = Modifier.padding(10.dp)) {
        Text(
            text = "${stringResource(id = R.string.humidity)} $value",
            color = Color.DarkGray,
            fontSize = 15.sp,
            fontWeight = FontWeight.Bold,
            modifier = Modifier.align(Alignment.CenterVertically),

            )
        Image(
            painter = painterResource(id = R.drawable.ic_humidity),
            contentDescription = "",
            Modifier.padding(start = 5.dp)
        )
    }

}
