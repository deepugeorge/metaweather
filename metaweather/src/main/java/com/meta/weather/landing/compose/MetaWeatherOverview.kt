package com.meta.weather.landing.compose

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.accompanist.pager.ExperimentalPagerApi
import com.google.accompanist.pager.HorizontalPager
import com.google.accompanist.pager.pagerTabIndicatorOffset
import com.google.accompanist.pager.rememberPagerState
import com.meta.weather.landing.constants.getLocation
import com.meta.weather.landing.model.Status
import com.meta.weather.landing.viewmodel.MetaWeatherViewModel
import com.meta.weather.landing.viewmodel.MetaWeatherViewModelFactory
import com.meta.weather.ui.theme.MetaWeatherTheme
import kotlinx.coroutines.launch

@ExperimentalPagerApi
@Composable
fun WeatherOverview() {
    val viewModel:MetaWeatherViewModel = viewModel(factory = MetaWeatherViewModelFactory())
    val pagerState = rememberPagerState()

    val coroutineScope = rememberCoroutineScope()
    val pages = remember { getLocation() }
   /* LaunchedEffect(INITIAL_API_CALL){
        viewModel.loadData(currentPage = pagerState.currentPage)
    }*/

    viewModel.loadData(currentPage = pagerState.currentPage)

    Column {
        ScrollableTabRow(
            selectedTabIndex = pagerState.currentPage,
            indicator = { tabPositions ->
                TabRowDefaults.Indicator(
                    Modifier.pagerTabIndicatorOffset(pagerState, tabPositions)
                )
            }
        ) {
            pages.forEachIndexed { index, location ->
                Tab(
                    text = { Text(location.locationName) },
                    selected = pagerState.currentPage == index,
                    onClick = {
                        coroutineScope.launch { pagerState.animateScrollToPage(index) }
                    },
                )
            }
        }

        HorizontalPager(
            count = pages.size,
            state = pagerState,
            modifier = Modifier.fillMaxHeight()
        ) { page ->
            WeatherData(viewModel = viewModel, page != currentPage, page)
        }
    }

}

@Composable
fun WeatherData(viewModel: MetaWeatherViewModel, isPageChanging: Boolean, page: Int) {

    val status = viewModel.responseStatus.observeAsState().value ?: Status.IN_PROGRESS
    if (status == Status.IN_PROGRESS || isPageChanging) {
        LinearProgressIndicator()
    } else if (status == Status.COMPLETED) {
        viewModel.weatherList.let {
            LazyColumn {
                items(it) { item ->
                    WeatherCard(weather = item)
                }

            }
        }
    } else {
        ErrorView {
            viewModel.loadData(page)
        }
    }
}


@ExperimentalPagerApi
@Preview(showBackground = true, showSystemUi = true)
@Composable
fun DefaultPreview() {
    MetaWeatherTheme {
        WeatherOverview()
    }
}