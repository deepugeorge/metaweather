package com.meta.weather.landing.compose

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.meta.weather.R

@Composable
fun DegreeCelciusLarge(temperature: String) {
    val annotatedString = buildAnnotatedString {
        withStyle(style = SpanStyle(color = Color.Black, fontSize = 30.sp)) {
            append(temperature)
            append(stringResource(id = R.string.degree))
        }
        withStyle(
            style = SpanStyle(
                color = Color.Gray,
                fontSize = 16.sp,
                fontWeight = FontWeight.Bold
            )
        ) {
            append(stringResource(id = R.string.alphabet_c))
        }
    }
    Text(
        text = annotatedString,
        fontWeight = FontWeight.Bold,
        modifier = Modifier
            .padding(10.dp)
            .wrapContentHeight()
    )
}

@Composable
fun DegreeCelciusSmall(prefix: String, temperature: String) {
    val annotatedString = buildAnnotatedString {
        withStyle(style = SpanStyle(color = Color.DarkGray, fontSize = 15.sp)) {
            append("$prefix ")
        }

        withStyle(style = SpanStyle(color = Color.Black, fontSize = 15.sp)) {
            append(temperature)
            append(stringResource(id = R.string.degree))
        }
        withStyle(
            style = SpanStyle(
                color = Color.Gray,
                fontSize = 10.sp,
                fontWeight = FontWeight.Bold
            )
        ) {
            append(stringResource(id = R.string.alphabet_c))
        }
    }
    Text(
        text = annotatedString,
        fontWeight = FontWeight.Bold,
        modifier = Modifier
            .padding(10.dp)
            .wrapContentHeight()
    )
}


@Preview(showBackground = true, showSystemUi = true)
@Composable
fun PreviewDegreeCelcius() {
    DegreeCelciusLarge(temperature = "25")
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
fun PreviewDegreeCelciusSmall() {
    DegreeCelciusSmall("High temperature", temperature = "20")
}