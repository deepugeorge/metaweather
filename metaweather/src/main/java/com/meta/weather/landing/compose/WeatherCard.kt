package com.meta.weather.landing.compose

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.rememberImagePainter
import com.meta.weather.R
import com.meta.weather.landing.model.Weather

@Composable
fun WeatherCard(weather: Weather) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp),
        elevation = 10.dp
    ) {
        Row(
            modifier = Modifier
                .padding(15.dp)
                .fillMaxWidth(1f)
        ) {
            Column(Modifier.fillMaxWidth(.65f)) {
                Text(
                    weather.date, modifier = Modifier.padding(10.dp),
                    style = TextStyle(
                        fontStyle = FontStyle.Italic,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 22.sp
                    )
                )

                DegreeCelciusSmall(
                    stringResource(id = R.string.hight_temperature),
                    weather.maximumTemperature.toString()
                )

                DegreeCelciusSmall(
                    stringResource(id = R.string.low_temperature),
                    weather.minimumTemperature.toString()
                )

                Humidity(weather.humidity)

            }
            Column {
                Image(
                    painter = rememberImagePainter(weather.imageUrl),
                    contentDescription = "",
                    modifier = Modifier
                        .padding(15.dp)
                        .size(40.dp),
                    alignment = Alignment.CenterEnd,

                    )

                Text(
                    weather.condition,
                    style = TextStyle(
                        fontSize = 18.sp
                    ),
                    modifier = Modifier.align(Alignment.CenterHorizontally)
                )

                DegreeCelciusLarge(temperature = weather.currentTemperature.toString())
            }

        }

    }
}


@Preview
@Composable
fun CardPreview() {
    val weather = Weather(
        "Raining",
        "sv",
        12,
        23,
        12,
        "2021-11-04",
        23
    )
    WeatherCard(weather = weather)
}