package com.meta.weather.landing.constants

enum class DateConstants(val date: String) {
    TODAY("Today"), TOMORROW("Tomorrow")
}