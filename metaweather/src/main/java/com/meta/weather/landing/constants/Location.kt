package com.meta.weather.landing.constants

private const val GOTHENBURG = "Gothenburg"
private const val STOCKHOLM = "Stockholm"
private const val MOUNTAIN_VIEW = "Mountain View"
private const val LONDON = "London"
private const val NEW_YORK = "New York"
private const val BERLIN = "Berlin"

data class Location(
    val locationName: String,
    val woeid: Int
)

fun getLocation(): List<Location> {
    return listOf(
        Location(
            GOTHENBURG, 890869
        ),
        Location(
            STOCKHOLM, 906057
        ),
        Location(
            MOUNTAIN_VIEW, 2455920
        ),
        Location(
            LONDON, 44418
        ),
        Location(
            NEW_YORK, 2459115
        ),
        Location(
            BERLIN, 638242
        )
    )
}