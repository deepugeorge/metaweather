package com.meta.weather.landing.model

import com.google.gson.annotations.SerializedName

data class ConsolidatedWeatherItem(
    @SerializedName("weather_state_name")
    val condition: String,
    @SerializedName("weather_state_abbr")
    val stateAbbreviation: String,
    @SerializedName("min_temp")
    val minTemperature: Double,
    @SerializedName("max_temp")
    val maxTemperature: Double,
    @SerializedName("the_temp")
    val currentTemperature: Double,
    @SerializedName("applicable_date")
    val date:String,
    val humidity:Int
)

data class ConsolidatedWeather(
    @SerializedName("consolidated_weather")
    val consolidatedWeatherItems: List<ConsolidatedWeatherItem> = emptyList()
)

data class Weather(
    val condition: String,
    val imageUrl:String,
    val minimumTemperature:Int,
    val maximumTemperature:Int,
    val currentTemperature:Int,
    val date:String,
    val humidity: Int
)

enum class Status {
    IN_PROGRESS,COMPLETED,ERROR
}
