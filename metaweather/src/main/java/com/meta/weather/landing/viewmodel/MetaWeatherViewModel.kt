package com.meta.weather.landing.viewmodel

import androidx.lifecycle.*
import com.app.network.result.Result
import com.meta.weather.landing.api.IMAGE_URL
import com.meta.weather.landing.api.TARGET_DATE_FORMAT
import com.meta.weather.landing.api.WEATHER_DATE_FORMAT
import com.meta.weather.landing.api.WeatherRepository
import com.meta.weather.landing.constants.DateConstants
import com.meta.weather.landing.constants.getLocation
import com.meta.weather.landing.model.Status
import com.meta.weather.landing.model.Weather
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import java.util.*
import kotlin.math.roundToInt

class MetaWeatherViewModel(
    private val repository: WeatherRepository,
    private val backgroundDispatcher: CoroutineDispatcher = Dispatchers.Default,
    private val currentDate: Calendar = Calendar.getInstance()
) : ViewModel() {

    private var currentJob:Job?= null

    private var _responseStatus = MutableLiveData<Status>()
    val responseStatus: LiveData<Status>
        get() = _responseStatus


    var weatherList = mutableListOf<Weather>()
        private set

    fun loadData(currentPage: Int) {
        cancelJob()

        _responseStatus.value = Status.IN_PROGRESS
        currentJob = viewModelScope.launch(backgroundDispatcher) {
            weatherList.clear()
            val response =
                repository.getWeatherOverviewList(getLocation()[currentPage].woeid)
            when (response) {
                is Result.Success -> {
                    val result = response.body.consolidatedWeatherItems.map { rawWeather ->
                        Weather(
                            condition = rawWeather.condition,
                            imageUrl = String.format(IMAGE_URL, rawWeather.stateAbbreviation),
                            minimumTemperature = rawWeather.minTemperature.roundToInt(),
                            maximumTemperature = rawWeather.maxTemperature.roundToInt(),
                            currentTemperature = rawWeather.maxTemperature.roundToInt(),
                            date = getFormattedDate(rawWeather.date),
                            humidity = rawWeather.humidity
                        )
                    }
                    weatherList.addAll(result)
                    _responseStatus.postValue(Status.COMPLETED)
                }

                // TODO - Handle different error types

                is Result.NetworkError -> {
                    _responseStatus.postValue(Status.ERROR)
                }
                is Result.ServerError -> {
                    _responseStatus.postValue(Status.ERROR)
                }
                is Result.UnknownError -> {
                    _responseStatus.postValue(Status.ERROR)
                }
            }
        }
    }

    private fun cancelJob() {
        if(currentJob?.isActive == true) {
            currentJob?.cancel()
        }
    }

    private fun getFormattedDate(source: String): String {

        return when {
            isSameDay(source = source) -> DateConstants.TODAY.date
            isTomorrow(source = source) -> DateConstants.TOMORROW.date
            else -> formattedDate(source)
        }

    }

    private fun formattedDate(source: String): String {
        val date = WEATHER_DATE_FORMAT.parse(source) ?: Date()
        return TARGET_DATE_FORMAT.format(date)
    }

    private fun isSameDay(source: String): Boolean {
        val currentDateString = WEATHER_DATE_FORMAT.format(currentDate.time)
        return currentDateString == source
    }

    private fun isTomorrow(source: String): Boolean {
        val nextDateCalendar = (currentDate.clone() as Calendar).apply { add(Calendar.DATE, 1) }
        val nextDateString = WEATHER_DATE_FORMAT.format(nextDateCalendar.time)
        return nextDateString == source
    }

    override fun onCleared() {
        cancelJob()
        super.onCleared()
    }

}

class MetaWeatherViewModelFactory :
    ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MetaWeatherViewModel::class.java)) {
            return MetaWeatherViewModel(WeatherRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}