package com.meta.weather

import com.app.network.result.Result
import com.google.gson.Gson
import java.io.File

internal object ResponseObjectProvider {
    private const val PATH_URL = "src/test/resources/mocks/weather/"


    fun <T : Any> getMockResponse(fileName: String, response: Class<T>): Result.Success<T> {
        val file = File("$PATH_URL${fileName}")
        Gson().fromJson(file.readText(), response)

        return Result.Success(Gson().fromJson(file.readText(), response), 200, null)
    }

}