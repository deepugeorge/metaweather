package com.meta.weather.landing.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.meta.weather.ResponseObjectProvider
import com.meta.weather.landing.api.WEATHER_DATE_FORMAT
import com.meta.weather.landing.api.WeatherRepository
import com.meta.weather.landing.model.ConsolidatedWeather
import com.meta.weather.rule.CoroutineTestRule
import io.mockk.coEvery
import io.mockk.mockk
import io.mockk.unmockkAll
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.util.*

class MetaConsolidatedWeatherViewModelTest {
    @Rule
    @JvmField
    val rule: InstantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutineTestRule()

    private val repository = mockk<WeatherRepository>()

    private lateinit var metaWeatherViewModel: MetaWeatherViewModel

    @Before
    fun onBefore() {
        val currentData =
            Calendar.getInstance().apply { time = WEATHER_DATE_FORMAT.parse("2021-11-05") }
        metaWeatherViewModel = MetaWeatherViewModel(
            repository = repository,
            coroutinesTestRule.testDispatcher,
            currentData
        )
    }

    @Test
    fun `verify the response contains the data`() {
        coEvery {
            repository.getWeatherOverviewList(890869)
        } returns
                ResponseObjectProvider.getMockResponse(
                    "success.json",
                    ConsolidatedWeather::class.java
                )
        metaWeatherViewModel.loadData(0)
        assertTrue(metaWeatherViewModel.weatherList[0].minimumTemperature == 3)
        assertEquals(
            "First item weather condition is heavy rain", "Heavy Cloud",
            metaWeatherViewModel.weatherList[0].condition
        )
        assertEquals(
            "First item should be todays data",
            "Today",
            metaWeatherViewModel.weatherList[0].date
        )
        assertEquals(
            "Second item should be tomorrow data","Tomorrow",
            metaWeatherViewModel.weatherList[1].date
        )

        assertEquals(
            "Third item should be formatted data","Sunday, Nov 7",
            metaWeatherViewModel.weatherList[2].date
        )

    }


    @After
    fun onAfter() {
        unmockkAll()
    }

}