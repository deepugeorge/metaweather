package com.app.cache

interface Cache {

    fun <K : CacheKey, V> save(key: K, va: V): Boolean

    fun <K : CacheKey, V> getResult(key: K): V?

    fun clear(key: CacheKey? = null)

    fun cacheSize(): Int

}

interface CacheKey