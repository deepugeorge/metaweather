package com.app.cache

object LocalCache : Cache {

    private var cache = mutableMapOf<Any, Any>()

    override fun <K : CacheKey, V> save(key: K, va: V): Boolean {
        return if (va != null) {
            cache[key] = va
            true
        } else false
    }

    override fun <K : CacheKey, V> getResult(key: K): V? = cache[key] as? V

    override fun clear(key: CacheKey?) {
        if (key != null) {
            cache.remove(key)
        } else {
            cache.clear()
        }
    }

    override fun cacheSize(): Int = cache.size


}