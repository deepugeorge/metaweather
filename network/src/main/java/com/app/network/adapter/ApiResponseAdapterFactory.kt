package com.app.network.adapter


import com.app.network.result.Result
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class ApiResponseAdapterFactory : CallAdapter.Factory() {
    override fun get(
        returnType: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (returnType !is ParameterizedType) return null

        val containerType = getParameterUpperBound(0, returnType)
        if (getRawType(containerType) != Result::class.java) return null

        if (containerType !is ParameterizedType) return null

        val (successBodyType, errorBodyType) = containerType.getBodyTypes()

        val errorBodyConverter = retrofit.nextResponseBodyConverter<Any>(null, errorBodyType, annotations)

        return when (getRawType(returnType)) {
            Deferred::class.java -> {
               DeferredResponseAdapter<Any, Any>(successBodyType, errorBodyConverter)
            }

            Call::class.java -> {
                CallResponseAdapter<Any, Any>(successBodyType, errorBodyConverter)
            }
            else -> null
        }
    }

    private fun ParameterizedType.getBodyTypes(): Pair<Type, Type> {
        val successType = getParameterUpperBound(0, this)
        val errorType = getParameterUpperBound(1, this)
        return successType to errorType
    }
}