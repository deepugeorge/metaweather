package com.app.network.adapter

import com.app.network.model.ApiError
import com.app.network.result.Result
import okhttp3.Request
import okhttp3.ResponseBody
import okio.Timeout
import retrofit2.*
import java.io.IOException
import java.lang.reflect.Type

class ApiResponseCall<SUCCESS : Any, ERROR : Any>(
    private val backingCall: Call<SUCCESS>,
    private val errorConverter: Converter<ResponseBody, ERROR>,
    private val successBodyType: Type
) : Call<Result<SUCCESS, ERROR>> {
    override fun clone(): Call<Result<SUCCESS, ERROR>> =
        ApiResponseCall(backingCall, errorConverter, successBodyType)

    override fun execute(): Response<Result<SUCCESS, ERROR>> =
        throw UnsupportedOperationException("Synchronous execution does not support")


    override fun isExecuted(): Boolean = backingCall.isExecuted

    override fun cancel() = backingCall.cancel()

    override fun isCanceled(): Boolean = backingCall.isCanceled

    override fun request(): Request = backingCall.request()

    override fun timeout(): Timeout = backingCall.timeout()

    override fun enqueue(callback: Callback<Result<SUCCESS, ERROR>>) {
        synchronized(this) {
            backingCall.enqueue(object : Callback<SUCCESS> {
                override fun onResponse(call: Call<SUCCESS>, response: Response<SUCCESS>) {
                    if (response.isSuccessful) {
                        val body = response.body()
                        if (body == null) {
                            callback.onResponse(
                                this@ApiResponseCall,
                                Response.success(Result.ServerError(body, response.code()))
                            )
                        } else {
                            callback.onResponse(
                                this@ApiResponseCall, Response.success(
                                    Result.Success(
                                        body,
                                        response.code(),
                                        response.headers()
                                    ) as Result<SUCCESS, ERROR>
                                )
                            )

                        }
                    } else {
                        val result = response.errorBody()?.run {
                            try {
                                errorConverter.convert(this)
                            } catch (exception: IOException) {
                                null
                            }

                        }
                        callback.onResponse(
                            this@ApiResponseCall, Response.success(
                                Result.ServerError(
                                    result,
                                    response.code(),
                                    response.headers()
                                )
                            )
                        )
                    }
                }

                override fun onFailure(call: Call<SUCCESS>, t: Throwable) {
                    val result = when (t) {
                        is IOException -> Result.NetworkError(t)
                        is HttpException -> {
                            val errorBody = t.response()?.errorBody() ?: "{}"
                            val errorCode = t.response()?.code() ?: 0
                            Result.ServerError(errorBody, errorCode)
                        }
                        else -> Result.UnknownError(t)
                    }
                    callback.onResponse(
                        this@ApiResponseCall,
                        Response.success(result as Result<SUCCESS, ERROR>)
                    )
                }

            })

        }
    }

}