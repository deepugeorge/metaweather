package com.app.network.adapter

import com.app.network.result.Result
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Converter
import java.lang.reflect.Type

class CallResponseAdapter<SUCCESS : Any, ERROR : Any>(
    private val successType: Type,
    private val errorConverter: Converter<ResponseBody, ERROR>
) : CallAdapter<SUCCESS, Call<Result<SUCCESS, ERROR>>> {
    override fun responseType(): Type = successType

    override fun adapt(call: Call<SUCCESS>): Call<Result<SUCCESS, ERROR>> =
        ApiResponseCall(call, errorConverter, successType)
}