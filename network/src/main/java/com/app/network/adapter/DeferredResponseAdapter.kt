package com.app.network.adapter

import com.app.network.result.Result
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.Deferred
import okhttp3.ResponseBody
import retrofit2.*
import java.io.IOException
import java.lang.reflect.Type

class DeferredResponseAdapter<SUCCESS : Any, ERROR : Any>(
    private val successBodyType: Type,
    private val errorConverter: Converter<ResponseBody, ERROR>
) :
    CallAdapter<SUCCESS, Deferred<Result<SUCCESS, ERROR>>> {
    override fun responseType(): Type = successBodyType
    override fun adapt(call: Call<SUCCESS>): Deferred<Result<SUCCESS, ERROR>> {
        val deferred = CompletableDeferred<Result<SUCCESS, ERROR>>()
        deferred.invokeOnCompletion {
            if (deferred.isCancelled) {
                call.cancel()
            }
        }
        call.enqueue(object : Callback<SUCCESS> {
            override fun onResponse(call: Call<SUCCESS>, response: Response<SUCCESS>) {
                if (response.isSuccessful) {
                    val body = response.body()
                    if (body == null) {
                        deferred.complete(Result.ServerError(body, response.code()))
                    } else {
                        deferred.complete(
                            Result.Success(
                                body,
                                response.code(),
                                response.headers()
                            ) as Result<SUCCESS, ERROR>
                        )
                    }
                } else {
                    val result = response.errorBody()?.run {
                        errorConverter.convert(this)
                    }
                    deferred.complete(
                        Result.ServerError(
                            result,
                            response.code(),
                            response.headers()
                        )
                    )

                }
            }

            override fun onFailure(call: Call<SUCCESS>, t: Throwable) {
                try {
                    val result = when (t) {
                        is IOException -> Result.NetworkError(t)
                        is HttpException -> {
                            val errorBody = t.response()?.errorBody() ?: "{}"
                            val errorCode = t.response()?.code() ?: 0
                            Result.ServerError(errorBody, errorCode)
                        }
                        else -> Result.UnknownError(t)
                    }
                    deferred.complete(result as Result<SUCCESS, ERROR>)
                } catch (t: Throwable) {
                    deferred.completeExceptionally(t)

                }
            }

        })
        return deferred
    }
}