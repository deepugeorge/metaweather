package com.app.network.configuration

import com.app.common.application.MainApplication
import com.app.network.adapter.ApiResponseAdapterFactory
import com.app.network.interceptor.LoggingInterceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkConfig {
    private val baseUrl = MainApplication.getInstance()?.baseUrl.orEmpty()

    private fun getRetroInstance(): Retrofit {


        val client = OkHttpClient.Builder().addInterceptor(LoggingInterceptor()).build()

        return Retrofit.Builder().baseUrl(baseUrl)
            .addCallAdapterFactory(ApiResponseAdapterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    fun <T> buildService(service: Class<T>): T {
        return getRetroInstance().create(service)
    }

}