package com.app.network.interceptor

import com.app.network.BuildConfig
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.stream.MalformedJsonException
import okhttp3.*
import okhttp3.ResponseBody.Companion.toResponseBody
import okhttp3.internal.http2.Http2Reader.Companion.logger
import java.util.*


class LoggingInterceptor : Interceptor {

    private val gson = GsonBuilder().setPrettyPrinting().create()


    override fun intercept(chain: Interceptor.Chain): Response {
        val request: Request = chain.request()

        logger.info("Request url ${request.url}")
        val response = chain.proceed(request)

        if (BuildConfig.DEBUG) {
            logger.info("Library package name ${BuildConfig.LIBRARY_PACKAGE_NAME}")

            val t1 = System.nanoTime()
            logger.info(
                String.format(
                    "Sending request %s on %s%n%s",
                    request.url, chain.connection(), request.headers
                )
            )


            val t2 = System.nanoTime()
            logger.info(
                String.format(
                    Locale.getDefault(),
                    "Received response for %s in %.1fms%n%s",
                    response.request.url, (t2 - t1) / 1e6, response.headers
                )
            )

            val content = response.body?.string() ?: "{}"
            val contentType: MediaType? = response.body?.contentType()

            logger.info(toPrettyFormat(content))



            val wrappedBody: ResponseBody = content.toResponseBody(contentType)
            return response.newBuilder().body(wrappedBody).build()
        }


        return response
    }

    private fun toPrettyFormat(jsonString: String?): String? {
        val parser = JsonParser()
        try {
            val json = parser.parse(jsonString).asJsonObject
            return gson.toJson(json)
        } catch (exception: Exception) {
            logger.info(exception.stackTraceToString())
        }
        return "{}"

    }
}