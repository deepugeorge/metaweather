package com.app.network.model

data class ApiError(
    override val errorMessage: String
) : IError

interface IError {
    val errorMessage: String
}