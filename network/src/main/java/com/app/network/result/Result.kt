package com.app.network.result

import okhttp3.Headers
import java.io.IOException

sealed class Result<out SUCCESS : Any, out ERROR  : Any> {

    data class Success<SUCCESS : Any>(
        val body: SUCCESS,
        val code: Int,
        val headers: Headers? = null
    ) : Result<SUCCESS, Nothing>()

    interface Error {
        val error: Throwable
    }

    /**
     * A request that resulted in a response with a non-2xx status code.
     */
    data class ServerError<ERROR : Any>(
        val body: ERROR?,
        val code: Int,
        val headers: Headers? = null
    ) : Result<Nothing, ERROR>(), Error {
        override val error = IOException("Network server error: $code \n$body")
    }

    /**
     * A request that didn't result in a response.
     */
    data class NetworkError(override val error: IOException) :
        Result<Nothing, Nothing>(), Error

    /**
     * Exception occurred when improper data received
     * Eg:-> Improper JSON
     */
    data class UnknownError(
        override val error: Throwable,
        val code: Int? = null,
        val headers: Headers? = null,
    ) : Result<Nothing, Nothing>(), Error
}