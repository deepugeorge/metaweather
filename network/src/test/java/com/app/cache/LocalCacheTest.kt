package com.app.cache

import junit.framework.Assert.assertTrue
import org.junit.After
import org.junit.Before
import org.junit.Test

class LocalCacheTest {
    lateinit var cache: Cache

    @Before
    fun onBefore() {
        cache = LocalCache
    }


    @Test
    fun `check the system to save the file`() {
        val data = CharArray(1000000)
        cache.save(TestCacheKey.DATA_TEST, data)
        assertTrue("One data present in the cache", cache.cacheSize() == 1)
        cache.save(TestCacheKey.DATA_TEST, data)
        assertTrue("Data re written", cache.cacheSize() == 1)
    }

    @After
    fun onAfter() {
        cache.clear()
    }
}

enum class TestCacheKey : CacheKey {
    DATA_TEST
}