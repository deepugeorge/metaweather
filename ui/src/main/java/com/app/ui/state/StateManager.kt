package com.app.ui.state

import com.app.network.model.IError
import com.app.network.result.Result
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

interface StateManagerViewModel {
    var networkState: StateFlow<NetworkState>
}

class StateManager(private val networkStateFlow: MutableStateFlow<NetworkState>) {
    fun mangeResult(result: Result<Any, IError>) {
        networkStateFlow.value = NetworkState(isLoading = false)
        when (result) {
            is Result.ServerError -> {
                networkStateFlow.value =
                    NetworkState(errorMessage = result.body?.errorMessage ?: "Incorrect request")
            }
            is Result.NetworkError -> {
                networkStateFlow.value =
                    NetworkState(errorMessage = "Network error occurred. Please check your connection")
            }
            is Result.Success -> {
                // DO NOTHING
            }

            else -> {
                networkStateFlow.value =
                    NetworkState(errorMessage = "Something went wrong. Please try again")
            }
        }
    }

    fun showLoading() {
        networkStateFlow.value = NetworkState(isLoading = true)
    }
}

data class NetworkState(
    var errorMessage: String = "",
    var isLoading: Boolean = false
)