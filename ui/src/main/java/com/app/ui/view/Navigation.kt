package com.app.ui.view

import androidx.navigation.NavHostController

fun popBack(navController: NavHostController) {
    navController.popBackStack()
}