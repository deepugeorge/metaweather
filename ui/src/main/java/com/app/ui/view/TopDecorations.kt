package com.app.ui.view

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Card
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.constraintlayout.compose.ConstraintLayout
import com.app.ui.R
import com.app.ui.state.StateManagerViewModel

@Preview
@Composable
private fun ErrorViewCard(errorMessage: String = "") {

    Card {
        Row(modifier = Modifier.fillMaxWidth()) {
            Image(
                painter = painterResource(R.drawable.ic_error_24),
                stringResource(R.string.error_image_icon_content_description),
                modifier = Modifier.align(Alignment.CenterVertically)
            )

            Text(
                modifier = Modifier.align(Alignment.CenterVertically),
                text = errorMessage,
                style = MaterialTheme.typography.subtitle1,
                color = MaterialTheme.colors.primaryVariant
            )
        }
    }

}

@Composable
fun ToLevelLayout(viewModel: StateManagerViewModel) {

    val networkState = viewModel.networkState.collectAsState().value
    val errorMessage = networkState.errorMessage

    if (networkState.isLoading) {
        LinearProgressIndicator(modifier = Modifier.fillMaxWidth())
    }

    if (errorMessage.isNotEmpty()) {
        ErrorViewCard(errorMessage)
    }
}


@Composable
fun NetworkResponsiveView(viewModel: StateManagerViewModel, content: @Composable () -> Unit) {

    ConstraintLayout {
        val (topComponents, dataContainer) = createRefs()

        Box(modifier = Modifier.constrainAs(topComponents) {
            top.linkTo(parent.top)
        }) {
            ToLevelLayout(viewModel = viewModel)
        }
        Box(Modifier.constrainAs(dataContainer) {
            top.linkTo(topComponents.bottom)
        }) {
            content()
        }

    }
}


